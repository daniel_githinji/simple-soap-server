/**
* Author : Larry Kiniu,
* Date : 24-11-2016
* Description : Orbit CBS Mockup for Advans Orange Money Integration Project
*/
var soap = require('soap');
var http = require('http');
var fs = require('fs');
var request = require('request');
var util = require('util');
// create a stdout and file logger
// create a custom timestamp format for log statements
var SimpleNodeLogger = require('simple-node-logger'),
	opts = {
		logFilePath:'debug.log',
		timestampFormat:'YYYY-MM-DD HH:mm:ss.SSS'
	},
log = SimpleNodeLogger.createSimpleLogger( opts );
//initialize log level
log.setLevel('trace');


var idType = {
    0001 :	"National ID Card",
	0002 :	"Passport",
	0003 :	"Drivers Licence",
	0004 :	"Employer ID Card",
	0005 :	"TSM Number",
	9999 :	"Business Registration Number"

}

var branches = {
	001 : "Letshego Head Office",
	002 : "Molepolole Satellite",
	003	: "Francistown Branch",
	004	: "Lobatse Satellite",
	005	: "Jwaneng Satellite",
	006	: "Palapye Branch",
	007	: "Maun Branch",
	008	: "Letlhakane Satellite",
	009	: "Ghanzi Satellite",
	010	: "Serowe Satellite",
	011	: "Selibe Phikwe Satellite",
	012	: "Kgale Direct Sales",
	013	: "Kasane Satellite",
	014	: "Tele Sales In-house",
	015	: "Mahalapye Satellite",
	016	: "Kanye Satellite",
	017	: "Tsabong Satellite",
	018	: "Mental Power - Broker",
	019	: "Oseg - Broker",
	020	: "Remi Investments - Broker",
	021	: "Freeway Security - Broker",
	022	: "Dikgoni - Broker",
	023	: "Flamingo - Broker",
	024	: "Kredicell - Broker",
	025	: "Freeway Security 2 - Broker",
	026	: "Crawley Holdings - Broker",
	029	: "The Modern Set (Pty) Ltd",
	999	: "Head Office-Budget"
}

var salutations = {
	01: "Mr",
	02: "Ms",
	03: "Mrs",
	04: "Miss",
	05: "Dr",
	06: "Prof."
}

var languages = {
	01 : "English",
	02 : "Portuguese"
}

var employmentStatus = {
	"F": "Full-time",
	"P": "Part-time",
	"R": "Pensioner",
	"S": "Self-employed", 
	"U": "Unemployed"
}

var maritalStatus = {
	"S":"Single",
	"M":"Married",
	"D":"Divorced",
	"C":"Defacto",
	"W":"Widowed",
	"L":"Living Apart",
	"N":"Never Married",
	"U":"Unknown"
}

var alertMode = {
	01: "Mobile",
	02: "Email",
	03: "Both",
	04: "Not Applicable"
}

var gender = {
	"M": "Male",
	"F": "Female",
	"NA": "Not Applicable"
}

var countries = {
	"BW" :   "Botswana",
	"BZ" :	"Belize",
	"CA" :	"Canada",
	"CD" :	"Congo, The Democratic Republic",
	"CF" :	"Central African Republic",
	"CH" :	"Switzerland",
	"CL" :	"Chile",
	"CM" :	"Cameroon",
	"CN" :	"China",
	"CY" :	"Cyprus"
}

var cities = {
	"A01" :	"Serowe",
	"A02" :	"Selebi Phikwe",
	"A03" :	"Mahalapye",
	"A04" :	"Palapye",
	"A05" :	"Bobonong",
	"A06" :	"Letlhakane",
	"A07" :	"Tonota",
	"A08" :	"Mmadinare",
	"A09" :	"Tutume"

}

var province = {
	"A00" :	"Central",
	"C00" :	"Kgalagadi",
	"D00" :	"Kgatleng",
	"E00" :	"Kweneng",
	"F00" :	"North-East",
	"G00" :	"North-West",
	"H00" :	"South-East",
	"J00" :	"Southern"
}


  var CustomerCreationRsHeader = {
    "ns2:Filler2": 00,
    "ns2:Filler3": 110,
    "ns2:Password": 0,
    "ns2:MsgNum": 0,
    "ns2:SegNum": 0,
    "ns2:SegNum2": 0,
	"ns2:TermNum": 0,
	"ns2:InstNum": 2,
	"ns2:BrchNum": 1,
	"ns2:WorkstationNum": 0,
	"ns2:TellerNum": 30003,
	"ns2:TrnNum": 062001,
	"ns2:JrnlNum": 99631,
	"ns2:RsHdrDt": 04072017,
	"ns2:Flag1": 0,
	"ns2:Flag2": 2,
	"ns2:Flag3": 0,
	"ns2:Flag4": 0,
	"ns2:Flag5": "Y",
	"ns2:UUIDSOURCE":"",
	"ns2:UUIDNUM": "",
	"ns2:UUIDSEQNUM": 0,
	"ns2:OutputType": 08,
}


  var PromptAmendRsHeader = {
    "ns2:Filler2": 00,
    "ns2:Filler3": 5905,
    "ns2:CycNum": 0,
    "ns2:MsgNum": 0,
    "ns2:SegNum": 0,
    "ns2:SegNum2": 0,
	"ns2:TermNum": 0,
	"ns2:InstNum": 2,
	"ns2:BrchNum": 1,
	"ns2:WorkstationNum": 0,
	"ns2:TellerNum": 30003,
	"ns2:TrnNum": 067000,
	"ns2:JrnlNum": 99654,
	"ns2:RsHdrDt": 04072017,
	"ns2:Flag1": 0,
	"ns2:Flag2": 6,
	"ns2:Flag3": 0,
	"ns2:Flag4": 0,
	"ns2:Flag5": "Y",
	"ns2:UUIDSOURCE":"",
	"ns2:UUIDNUM": "",
	"ns2:UUIDSEQNUM": 0,
	"ns2:OutputType": 03,
 }

var promptAmendCustomerData = {
	"ns2:CustNum": 2010175241,
	"ns2:CustTyp": 01,
	"ns2:RelnshpMgr": 0,
	"ns2:Title": 1,
	"ns2:CustFirstName": "RENDÀO",
	"ns2:CustLastName": "SUSSÈ",
	"ns2:BussName": "",
	"ns2:Addr1": "sonewhere",
	"ns2:Addr2": "",
	"ns2:Addr3": "",
	"ns2:Addr4": "",
	"ns2:PstCode": "",
	"ns2:Dmcle": "BW",
	"ns2:HomeNum": "",
	"ns2:FaxNum": "",
	"ns2:BussNum": "",
	"ns2:Natlty": "BW",
	"ns2:HomeOwnership": "",
	"ns2:LangCode": 1,
	"ns2:IdNum": 967418631,
	"ns2:IdIssueDt": 02032000,
	"ns2:IdIssueAt": "",
	"ns2:IdTyp": 1,
	"ns2:DmstcRisk": "",
	"ns2:CrossBrdrRisk": "ZZ",
	"ns2:VipCust": 0,
	"ns2:Stat": 000,
	"ns2:SegCode": 172,
	"ns2:TfnProvided": 0,
	"ns2:HomeBrch": 1,
	"ns2:CustLmt": "",
	"ns2:Parent": "",
	"ns2:Indust": "",
	"ns2:Country": "",
	"ns2:Sec": "",
	"ns2:IndCode1": "",
	"ns2:CountryRisk": "",
	"ns2:GrpCode": "",
	"ns2:BussSecCode": "",
	"ns2:CorpGrpCode": "",
	"ns2:CustMidName": "",
	"ns2:CityCode": "",
	"ns2:WthldngTaxExmptn": "",
	"ns2:IvrFlag": "",
	"ns2:KycFlag": "",
	"ns2:KyDtChng": 04072017,
	"ns2:PassportCountry": "",
	"ns2:Email": "",
	"ns2:MobileNum": 74066923,
	"ns2:IdExpryDt": 28032027,
	"ns2:BlcklstngComnt": "",
	"ns2:Gndr": "M",
	"ns2:GrntrLnkdProcFlag": "",
	"ns2:Flag1": "",
	"ns2:Flag2": "",
	"ns2:Flag3": "",
	"ns2:Flag4": "",
	"ns2:Flag5": "",
	"ns2:Attr1": "",
	"ns2:Attr2": "",
	"ns2:Attr3": "00000000",
	"ns2:Attr4": "00000000",
	"ns2:Attr5": "",
	"ns2:Attr6": "",
	"ns2:Attr7": "",
	"ns2:Attr8": "",
	"ns2:Attr9": "0.000",
	"ns2:Attr11": "",
	"ns2:Attr10": "0.000",
	"ns2:Attr12": "",
	"ns2:SpecProvsn": "0.00",
	"ns2:LegalStat": "",
	"ns2:CustRiskGrd": "",
	"ns2:EmplTyp": "A",
	"ns2:StaffID": "",
	"ns2:SpouseName": "",
	"ns2:SpouseIdNo": "",
	"ns2:IdCode": "0",
	"ns2:MaritalStat": "S",
	"ns2:DOB": "28031997",
	"ns2:CoopJoinDt": "",
	"ns2:NetIncome": "0.000",
	"ns2:DfltStr": "",
	"ns2:AncPwr": "",
	"ns2:HomeVillage": "",
	"ns2:HomeWard": "",
	"ns2:Headman": "",
	"ns2:MobNetwrk": "",
	"ns2:Initials": "",
	"ns2:SpouseMaidenName": "",
	"ns2:SpouseDOB": "",
	"ns2:SpouseMarriageDt": "",
	"ns2:SpouseMobNum": "",
	"ns2:SpouseHomeTelNum": "",
	"ns2:SpouseWorkTelNum": "",
	"ns2:SpouseEmployer": "",
	"ns2:SpouseGrossMnthlyIncome": "2000.000",
	"ns2:SpouseAddr1": "",
	"ns2:SpouseAddr2": "",
	"ns2:SpouseAddr3": "",
	"ns2:KinName1": "",
	"ns2:KinReln1": "",
	"ns2:Kin1HomeTelNum1": "",
	"ns2:Kin1WorkTelNum1": "",
	"ns2:Kin1Mob1": "",
	"ns2:Kin1Employer1": "",
	"ns2:Kin1Add1": "",
	"ns2:Kin1Add2": "",
	"ns2:Kin1Add3": "",
	"ns2:kin2Name1": "",
	"ns2:Kin2Reln1": "",
	"ns2:Kin2HomeTelNum1": "",
	"ns2:Kin2WorkTelNum1": "",
	"ns2:Kin2Mob1": "",
	"ns2:Kin2Employer1": "",
	"ns2:kin2Add1": "",
	"ns2:kin2Add2": "",
	"ns2:kin2Add3": "",
	"ns2:Acctnum": "",
	"ns2:AcctTyp": "",
	"ns2:SpouseIdCode": "",
	"ns2:CardTyp": "",
	"ns2:CardNum": "",
	"ns2:CardExpDt": "",
	"ns2:BankCode": 0,
	"ns2:BrchCode": 0,
	"ns2:AcctName": "",
	"ns2:BankName": "",
	"ns2:BrchName": "",
	"ns2:EmplStat": "F",
	"ns2:AppDt": "",
	"ns2:Posn": "",
	"ns2:DtRetrmnt": "",
	"ns2:GrossMnthlyIncome": "2000.000",
	"ns2:SupName": "",
	"ns2:SupDesgn": "",
	"ns2:SupTelNum": "",
	"ns2:Occupaction": "T",
	"ns2:PlaceOfBirth": "",
	"ns2:MarraigeCont": "",
	"ns2:MaidenName": "",
	"ns2:Edu": "",
	"ns2:PermPrvnc": "A00",
	"ns2:PermHowLong": "2.00",
	"ns2:CommAdd1": "Namibia",
	"ns2:CommAdd2": "",
	"ns2:CommAdd3": "",
	"ns2:CommAdd4": "",
	"ns2:CommProv": "",
	"ns2:CommCity": "",
	"ns2:CommCountry": "",
	"ns2:CommPstCode": "",
	"ns2:IDD": "",
	"ns2:ContactName1": "",
	"ns2:ContactAdd1": "",
	"ns2:ContactPosn": "",
	"ns2:IDD1": "+258",
	"ns2:ContactTel1": "",
	"ns2:ContactName2": "",
	"ns2:ContactAdd2": "",
	"ns2:IDD2": "",
	"ns2:ContactTel2": "",
	"ns2:LicenseNum": "",
	"ns2:HowlongYrs": "0.00",
	"ns2:CurrDepCode": "",
	"ns2:CurrDepName": "",
	"ns2:PhotoCapDt": "",
	"ns2:BiometricCapDt": "",
	"ns2:BiometricCapTime": "0",
	"ns2:IDD3": "",
	"ns2:IDD4": "",
	"ns2:IDD6": "",
	"ns2:IDD5": "",
	"ns2:IDD8": "",
	"ns2:IDD7": "",
	"ns2:IDD9": "+258",
	"ns2:IDD10": "",
	"ns2:IDD11": "",
	"ns2:IDD12": "",
	"ns2:AlertCode": "01",
	"ns2:IDD13": "",
	"ns2:BiometricFirstCapDt": "",
	"ns2:BiometricFirstCapTime": 0,
	"ns2:ChngInd": "",
	"ns2:ClntLegNum": "",
	"ns2:GrpFlag": "N",
	"ns2:GrpName": ""
}

var enquireAccountDetailsResponse = {
	"ns2:CustNum": "00000002000882522",
	"ns2:CustTyp": "01",
	"ns2:RelnshpMgr": "0",
	"ns2:Title": 4,
	"ns2:Name": "Miss Shalenyana Mongale",
	"ns2:Addr1": "P.O. Box 1208",
	"ns2:Addr2": "Molepolole",
	"ns2:Addr3": "",
	"ns2:Addr4": "",
	"ns2:PstCode": "",
	"ns2:HomePhnNum": "73846231",
	"ns2:FaxNum": "",
	"ns2:CountryOfRes": "BW",
	"ns2:BussPhnNum": 5920145,
	"ns2:MobileNum": 73846231,
	"ns2:Natlty": "BW",
	"ns2:NumOfChqbk": "00",
	"ns2:NumOfCard": "00",
	"ns2:FacltyCnt": "0000",
	"ns2:CustLmt": "0.00",
	"ns2:TtlBal": "-35527",
	"ns2:CurCode1": "BWP",
	"ns2:ResInd": "",
	"ns2:UsrName": "NO TELLER NAME",
	"ns2:StmtFreq": "00",
	"ns2:StmtDay": "00",
	"ns2:GrpCode": "",
	"ns2:AvgEodBal": "0.00",
	"ns2:FatherName": "",
	"ns2:BlackLstInd": "N",
	"ns2:EmplInd": "",
	"ns2:BlackLstStat": "0",
	"ns2:BlackLstComnt": "",
	"ns2:CreditRtng": "0",
	"ns2:CustStat": "0",
	"ns2:IdNum": 907727005,
	"ns2:VipStat": "0",
	"ns2:AccumCnt1": 0,
	"ns2:AccumCnt2": 0,
	"ns2:AccumCnt3": 0,
	"ns2:AccumCnt4": 0,
	"ns2:AccumCnt5": 0,
	"ns2:AccumCnt6": 0,
	"ns2:AccumCnt7": 0,
	"ns2:AccumCnt8": 0,
	"ns2:AccumCnt9": 0,
	"ns2:AccumCnt10": 0,
	"ns2:EmailAdd": "",
	"ns2:LagCode": 1,
	"ns2:BrchCode": 2,
	"ns2:RtnMailInd": "",
	"ns2:HldMail": "",
	"ns2:Typ": "",
	"ns2:VipTyp": "",
	"ns2:name1": "Miss",
	"ns2:name2": "",
	"ns2:CreateDt": "01082014",
	"ns2:MemShpLnth": "676 YEARS  793 MONTHS",
	"ns2:CurrAge": "049 YEARS  003 MONTHS",
	"ns2:ServLnth": "014 YEARS  002 MONTHS",
	"ns2:RemainServ": "010 YEARS  009 MONTHS",
	"ns2:AvailChnl1": "",
	"ns2:AvailChnl2": "",
	"ns2:AvailChnl3": "",
	"ns2:AvailChnl4": "",
	"ns2:AvailChnl5": "",
	"ns2:AvailChnl6": "",
	"ns2:AvailChnl7": "",
	"ns2:AvailChnl8": "",
	"ns2:BioEnrollDt": 99999999,
	"ns2:BioEnrollTime": "00:00:00",
	"ns2:KYCFlag": "",
	"ns2:IdTyp": "0001",
	"ns2:Fac": "",
	"ns2:Coll": [{
		"ns2:AcctNum": 10200001008,
		"ns2:Stat": "AUTO",
		"ns2:ChqbkFlag": "",
		"ns2:CardFlag": "",
		"ns2:AcctFlagTyp": "L",
		"ns2:ProdDescptn": "Mig. Payroll  60M 150 000",
		"ns2:CurCode2": "BWP",
		"ns2:RelnshpWtAcct": "OWN",
		"ns2:Bal": "0.00",
		"ns2:OvrdrftAcctLmt": 25788.97,
		"ns2:AcctTyp": 4000,
		"ns2:AcctSubTyp": "0601",
		"ns2:ShrtName": "",
		"ns2:InstNum": "002",
		"ns2:TagStat": "",
		"ns2:LonOdAmt": 25788.97,
		"ns2:ArrAcctAmt":" 0.00",
		"ns2:OdInd": "",
		"ns2:HowLong": 4.17,
		"ns2:IBAN": "",
		"ns2:AcctBrch": 2,
		"ns2:CreationDt": 30052013,
		"ns2:Acctstat": 37,
		"ns2:PurpCod": 4
	},
	{
		"ns2:AcctNum": 10200238032,
		"ns2:Stat": "AUTO",
		"ns2:ChqbkFlag": "",
		"ns2:CardFlag": "",
		"ns2:AcctFlagTyp": "L",
		"ns2:ProdDescptn": "Mig. Payroll  60M 150 000",
		"ns2:CurCode2": "BWP",
		"ns2:RelnshpWtAcct": "OWN",
		"ns2:Bal": "0.00",
		"ns2:OvrdrftAcctLmt": 26402.07,
		"ns2:AcctTyp": 4000,
		"ns2:AcctSubTyp": "0601",
		"ns2:ShrtName": "",
		"ns2:InstNum": "002",
		"ns2:TagStat": "",
		"ns2:LonOdAmt": 26402.07,
		"ns2:ArrAcctAmt": "0.00",
		"ns2:OdInd": "",
		"ns2:HowLong": 3.03,
		"ns2:IBAN": "",
		"ns2:AcctBrch": 2,
		"ns2:CreationDt": 24072014,
		"ns2:Acctstat": 37,
		"ns2:PurpCod": 4
	}
	]

}

var mybancsService = {
    bancsService: {
        bancsServicePort: {
            customerSearch: function(args, cb, headers, req) {
                log.info('SOAP customerSearch request from ' + req.connection.remoteAddress);
                log.info('***********************');
                log.info(args);
                log.info('***********************');
                return {
                    customerSearchRes: {
                        response: response,
                        HomeBrchNum:Math.floor(Math.random() * 30) + 1,
						CustTyp:Math.floor(Math.random() * 30) + 1,
						Gndr:Object.keys(gender)[Math.floor(Math.random() * 3) + 1],
						TitleCode:Math.floor(Math.random() * 6) + 1,
						CustFirstName:"FirstName",
						CustLastName:"LastName",
						DOB:"1990/09/26",
						Natlty:"",
						Language:Object.keys(languages)[Math.floor(Math.random() * 2) + 1],
						MaritalStat:Object.keys(maritalStatus)[Math.floor(Math.random() * 8) + 1],
						SegCode:"0101",
						IdTyp:idType[Math.floor(Math.random() * 30) + 1],
						IdNum:Math.floor(Math.random() * 100000000) + 1,
						Addr1:"",
						CityCode:Object.keys(cities)[Math.floor(Math.random() * 9) + 1],
						PermPrvnc:Object.keys(province)[Math.floor(Math.random() * 8) + 1],
						CountryCode:Object.keys(countries)[Math.floor(Math.random() * 10) + 1],
						AlertCode:Math.floor(Math.random() * 4) + 1,
						MobileNum:Math.floor(Math.random() * 100000000) + 1,
						Photo:"",
						Signature:"",
						EmpStat:Object.keys(employmentStatus)[Math.floor(Math.random() * 5) + 1],
						LangCode:Math.floor(Math.random() * 2) + 1,
						DmstcRisk:"ZZ",
						XbrdrRisk:"ZZ",
						BrkrStat:"000",
						KycFlag:"",
						TfnFlag:0,
						ExtbltyFld9:Math.floor(Math.random() * 100000000) + 1,
						ExtbltyFld10:Math.floor(Math.random() * 100000000) + 1,
						IDD1:"+258",
						IDD9:"+258"
                    }
                };
            },
            createCustomer: function(args, cb, headers, req) {
                console.log('SOAP  request from ' + req.connection.remoteAddress);
                return {
                    CustAddRs: {
                        "ns2:RsHeader": CustomerCreationRsHeader,
                        "ns2:Stat": {
							"ns2:OkMessage": {
							   "ns2:RcptData": "O.K. 002 201017524-1 00001 000 00030003",
							   "ns2:Filler2": "O.K. 002",
								"ns2:CustNum": "201017524-1",
								"ns2:Filler1": "0000",
								"ns2:AcctNum": "201017524-1"
							}
						} 
                    }
                };
            },
			promptAmendCustomerDetails: function(args, cb, headers, req) {
                console.log('SOAP  request from ' + req.connection.remoteAddress);
                return {
                    ModCustDetailsPrmptRs: {
                        "ns2:RsHeader": PromptAmendRsHeader,
                        "ns2:ModCustDetailsPrmptData":  promptAmendCustomerData
                    }
                };
            },
			enquireAccountDetails: function(args, cb, headers, req) {
                console.log('SOAP  request from ' + req.connection.remoteAddress);
                return {
                    AcctDetailsInqRs: {
                        "ns2:RsHeader": PromptAmendRsHeader,
                        "ns2:AcctDetailsInqData":  enquireAccountDetailsResponse
                    }
                };
            },
            dictionaries: function(args, cb, headers, req) {
                console.log('SOAP  request from ' + req.connection.remoteAddress);
				log.info(args);
				return {
                    dictionariesRes: {
                        response: response,
						dic:[
						{key: Math.floor(Math.random() * 25) + 1,value: "Test"},
						{key: Math.floor(Math.random() * 25) + 1,value: "Test2"},
						{key: Math.floor(Math.random() * 25) + 1,value: "Test3"}
						]
                    }
                };
            }
        }
    }
}
  
  var args = {name: 'value'};
  soap.createClient('http://127.0.0.1:7002/bancsService?wsdl', function(err, client) {
      
	  if(err){
		  //console.log("describing the error");
		  log.error(err);
	  }else{
		 //console.log(client.describe()); 
	  }

  });
  

var xml = fs.readFileSync('ws.wsdl', 'utf8');
server = http.createServer(function(request,response) {
	log.info(response);

	if(request.url.indexOf('.xsd') !== -1){
		response.writeHead(200, {"Content-Type": "text/xml"});
		var path = request.url.substring(1);
		response.write(fs.readFileSync(path, 'utf8'));
		response.end(); 
	}else{
		response.end("404: Not Found: " + request.url);  
	}
});

  server.listen(7002);

  soap.listen(server, '/bancsService', mybancsService, xml, function(){
	  server.log = function(type, data) {
			//console.log(type);
		};
		server.on('headers', function(headers, methodName) {
			// It is possible to change the value of the headers 
			// before they are handed to the service method. 
			// It is also possible to throw a SOAP Fault 
			//console.log(headers);
		});
  });
  log.info('Server is running');
  
  
  